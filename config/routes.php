<?php
use Cake\Routing\Router;

Router::plugin(
    'TestFoo',
    ['path' => '/test-foo'],
    function ($routes) {
        $routes->fallbacks('DashedRoute');
    }
);
